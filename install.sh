#!/bin/bash

echo "Installing..."
cp vimrc ~/.vimrc
if [ ! -d ~/.vim ]; then
    echo "Creating directory: ~/.vim"
    mkdir ~/.vim/
fi
cp .vim/plugins.vim ~/.vim/
echo "Done."
